<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.squashtest.tm.plugin</groupId>
  <artifactId>report.qualitativecoverage</artifactId>
  <version>4.0.0.IT1-SNAPSHOT</version>
  <description>Standard (default) reports for Squash TM</description>

  <properties>
    <jasperreports.build.outputDirectory>${project.build.outputDirectory}/jasperreports</jasperreports.build.outputDirectory>
    <jasperreports.version>6.1.1</jasperreports.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <squash.core.version>3.0.0.RELEASE</squash.core.version>
  </properties>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.squashtest.tm</groupId>
        <artifactId>squash-tm-bom</artifactId>
        <version>${squash.core.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>
    <!--======= SQUASHTEST MODULES ======== -->
    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>core.report.api</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>

    <dependency>
      <groupId>net.sf.jasperreports</groupId>
      <artifactId>jasperreports</artifactId>
      <version>${jasperreports.version}</version>
      <scope>provided</scope>
      <optional>true</optional>
    </dependency>

    <dependency>
      <groupId>org.squashtest.tm</groupId>
      <artifactId>tm.service</artifactId>
      <version>${squash.core.version}</version>
      <scope>provided</scope>
    </dependency>
  </dependencies>

  <build>
    <resources>
      <resource>
        <filtering>true</filtering>
        <directory>src/main/resources</directory>
      </resource>
    </resources>

    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-release-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
      </plugins>
    </pluginManagement>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>com.alexnederlof</groupId>
        <artifactId>jasperreports-plugin</artifactId>
        <version>1.9</version>
        <configuration>
          <outputDirectory>${jasperreports.build.outputDirectory}</outputDirectory>
        </configuration>
        <dependencies>
          <dependency>
            <!-- required to make sure jr runtime version is in sync with compile-time version -->
            <groupId>net.sf.jasperreports</groupId>
            <artifactId>jasperreports</artifactId>
            <version>${jasperreports.version}</version>
            <exclusions>
              <exclusion>
                <!-- without this, old 1.3 version may be pulled outta nowhere and warious problems occur -->
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
              </exclusion>
            </exclusions>
          </dependency>
          <dependency>
            <!-- without this, old 1.3 version may be pulled outta nowhere and reports wont compile -->
            <groupId>xml-apis</groupId>
            <artifactId>xml-apis</artifactId>
            <version>1.4.01</version>
          </dependency>
        </dependencies>
        <executions>
          <execution>
            <goals>
              <goal>jasper</goal>
            </goals>
            <phase>prepare-package</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <groupId>com.google.code.sortpom</groupId>
        <artifactId>maven-sortpom-plugin</artifactId>
        <version>2.2.1</version>
        <configuration>
          <predefinedSortOrder>custom_1</predefinedSortOrder>
          <lineSeparator>\n</lineSeparator>
          <encoding>${project.build.sourceEncoding}</encoding>
          <sortProperties>true</sortProperties>
          <keepBlankLines>true</keepBlankLines>
          <sortDependencies>scope, artifactId</sortDependencies>
          <nrOfIndentSpace>2</nrOfIndentSpace>
          <expandEmptyElements>false</expandEmptyElements>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>sort</goal>
            </goals>
            <phase>verify</phase>
          </execution>
        </executions>
      </plugin>

      <plugin>
        <!-- Checks license headers throughout the project -->
        <groupId>com.mycila</groupId>
        <artifactId>license-maven-plugin</artifactId>
        <version>2.11</version>
        <inherited>false</inherited>
        <configuration>
          <aggregate>true</aggregate>
          <strictCheck>true</strictCheck>
          <header>src/material/header.txt</header>
          <properties>
            <license.yearSpan>${project.inceptionYear} - 2021</license.yearSpan>
            <license.copyrightOwner>${project.organization.name}</license.copyrightOwner>
          </properties>
          <excludes>
            <exclude>template.mf</exclude>
            <exclude>**/pom.xml</exclude>
            <exclude>**/pom.xml.*</exclude>
            <exclude>.settings</exclude>
            <exclude>build.properties</exclude>
            <exclude>.hgignore</exclude>
            <exclude>**/*.sql</exclude>
            <exclude>.hgtags</exclude>
            <exclude>.classpath</exclude>
            <exclude>.project</exclude>
            <exclude>**/material/**</exclude>
            <exclude>META-INF/**</exclude>
          </excludes>
          <mapping>
            <tag>DYNASCRIPT_STYLE</tag>
            <jrxml>XML_STYLE</jrxml>
          </mapping>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <inceptionYear>2010</inceptionYear>
  <organization>
    <name>Henix, henix.fr</name>
    <url>http://www.squashtest.org</url>
  </organization>
  <scm>
    <connection>scm:git:${project.basedir}</connection>
    <developerConnection>scm:git:${project.basedir}</developerConnection>
    <tag>HEAD</tag>
  </scm>

  <repositories>
    <repository>
      <id>squash</id>
      <url>http://repo.squashtest.org/maven2/releases</url>
    </repository>
    <repository>
      <id>squash-snapshots</id>
      <url>http://repo.squashtest.org/maven2/snapshots</url>
    </repository>
  </repositories>

  <distributionManagement>
    <repository>
      <id>squash-release-deploy-repo</id>
      <name>Squash releases deployment repo</name>
      <url>${deploy-repo.release.url}</url>
    </repository>
    <snapshotRepository>
      <id>squash-snapshot-deploy-repo</id>
      <name>Squash snapshot deployment repo</name>
      <url>${deploy-repo.snapshot.url}</url>
    </snapshotRepository>
  </distributionManagement>

</project>
