<?xml version="1.0" encoding="UTF-8"?>
<!--

    ====
            This file is part of the Squashtest platform.
            Copyright (C) 2010 - 2015 Henix, henix.fr

            See the NOTICE file distributed with this work for additional
            information regarding copyright ownership.

            This is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            this software is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this software.  If not, see <http://www.gnu.org/licenses/>.
    ====

        This file is part of the Squashtest platform.
        Copyright (C) 2010 - 2021 Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="qualitative-coverage-table-iterations" pageWidth="776" pageHeight="595" orientation="Landscape" columnWidth="736" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" resourceBundle="/WEB-INF/messages/tm/messages" whenResourceMissingType="Key" uuid="50a76b51-aced-4b94-8ed1-f97dc9aa4dc9">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="name" mode="Opaque" forecolor="#000000" backcolor="#ABCAE5" hAlign="Left" vAlign="Middle" isBold="true"/>
	<parameter name="subReportTableTestSuites" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="subReportTableRequirements" class="net.sf.jasperreports.engine.JasperReport"/>
	<parameter name="project" class="java.lang.String"/>
	<parameter name="allowsSettled" class="java.lang.Boolean"/>
	<parameter name="allowsUntestable" class="java.lang.Boolean"/>
	<parameter name="campaign" class="java.lang.String"/>
	<parameter name="rowcount" class="java.util.Map" isForPrompting="false"/>
	<field name="iterationName" class="java.lang.String"/>
	<field name="iterationOrder" class="java.lang.Long"/>
	<field name="requirements" class="java.util.List"/>
	<field name="testSuites" class="java.util.List"/>
	<detail>
		<band height="20">
			<subreport>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="776" height="20" uuid="1fc22d7f-44de-4dfa-882a-51323404515a"/>
				<subreportParameter name="project">
					<subreportParameterExpression><![CDATA[$P{project}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsSettled">
					<subreportParameterExpression><![CDATA[$P{allowsSettled}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsUntestable">
					<subreportParameterExpression><![CDATA[$P{allowsUntestable}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="rowcount">
					<subreportParameterExpression><![CDATA[$P{rowcount}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="objectName">
					<subreportParameterExpression><![CDATA[$R{report.qualitativecoverage.label.short.iteration2}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="campaign">
					<subreportParameterExpression><![CDATA[$P{campaign}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="REPORT_RESOURCE_BUNDLE">
					<subreportParameterExpression><![CDATA[$P{REPORT_RESOURCE_BUNDLE}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="iteration">
					<subreportParameterExpression><![CDATA[$F{iterationName}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{requirements})]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{subReportTableRequirements}]]></subreportExpression>
			</subreport>
		</band>
		<band height="20">
			<subreport>
				<reportElement stretchType="RelativeToTallestObject" x="0" y="0" width="776" height="20" isRemoveLineWhenBlank="true" uuid="b9fd367e-dd69-434e-87cb-b3a5ac277fd6">
					<printWhenExpression><![CDATA[($F{testSuites}.size() >0)]]></printWhenExpression>
				</reportElement>
				<subreportParameter name="project">
					<subreportParameterExpression><![CDATA[$P{project}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsSettled">
					<subreportParameterExpression><![CDATA[$P{allowsSettled}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="allowsUntestable">
					<subreportParameterExpression><![CDATA[$P{allowsUntestable}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="rowcount">
					<subreportParameterExpression><![CDATA[$P{rowcount}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableRequirements">
					<subreportParameterExpression><![CDATA[$P{subReportTableRequirements}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="campaign">
					<subreportParameterExpression><![CDATA[$P{campaign}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="subReportTableIterations">
					<subreportParameterExpression><![CDATA[$P{subReportTableTestSuites}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="REPORT_RESOURCE_BUNDLE">
					<subreportParameterExpression><![CDATA[$P{REPORT_RESOURCE_BUNDLE}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="iteration">
					<subreportParameterExpression><![CDATA[$F{iterationName}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new net.sf.jasperreports.engine.data.JRBeanCollectionDataSource($F{testSuites})]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{subReportTableTestSuites}]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
