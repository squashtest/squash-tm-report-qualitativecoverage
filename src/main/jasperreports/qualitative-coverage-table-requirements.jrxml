<?xml version="1.0" encoding="UTF-8"?>
<!--

    ====
            This file is part of the Squashtest platform.
            Copyright (C) 2010 - 2015 Henix, henix.fr

            See the NOTICE file distributed with this work for additional
            information regarding copyright ownership.

            This is free software: you can redistribute it and/or modify
            it under the terms of the GNU Lesser General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            this software is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU Lesser General Public License for more details.

            You should have received a copy of the GNU Lesser General Public License
            along with this software.  If not, see <http://www.gnu.org/licenses/>.
    ====

        This file is part of the Squashtest platform.
        Copyright (C) 2010 - 2021 Henix, henix.fr

        See the NOTICE file distributed with this work for additional
        information regarding copyright ownership.

        This is free software: you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        this software is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this software.  If not, see <http://www.gnu.org/licenses/>.

-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="qualitative-coverage-table-requirements" pageWidth="776" pageHeight="595" orientation="Landscape" columnWidth="736" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" resourceBundle="/WEB-INF/messages/tm/messages" whenResourceMissingType="Key" uuid="d9051855-5fe2-480a-a2cf-f894eebc61a6">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="name" mode="Opaque" hAlign="Center" vAlign="Middle">
		<box>
			<topPen lineWidth="1.0"/>
			<leftPen lineWidth="1.0"/>
			<bottomPen lineWidth="1.0"/>
			<rightPen lineWidth="1.0"/>
		</box>
		<conditionalStyle>
			<conditionExpression><![CDATA[((Integer)($P{rowcount}.get("rowcount"))%4 < 2)
/*it is so because the expression that increments the rowcount is evaluated twice
for no apparent reasons*/]]></conditionExpression>
			<style backcolor="#B3B2DA"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[((Integer)($P{rowcount}.get("rowcount"))%2 >= 2)
/*it is so because the expression that increments the rowcount is evaluated twice
for no apparent reasons*/]]></conditionExpression>
			<style backcolor="#FFFFFF"/>
		</conditionalStyle>
	</style>
	<style name="small_name" mode="Opaque" hAlign="Center" vAlign="Middle" fontSize="8">
		<box>
			<topPen lineWidth="1.0"/>
			<leftPen lineWidth="1.0"/>
			<bottomPen lineWidth="1.0"/>
			<rightPen lineWidth="1.0"/>
		</box>
		<conditionalStyle>
			<conditionExpression><![CDATA[((Integer)($P{rowcount}.get("rowcount"))%4 < 2)
/*it is so because the expression that increments the rowcount is evaluated twice
for no apparent reasons*/]]></conditionExpression>
			<style backcolor="#B3B2DA"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[((Integer)($P{rowcount}.get("rowcount"))%2 >= 2)
/*it is so because the expression that increments the rowcount is evaluated twice
for no apparent reasons*/]]></conditionExpression>
			<style backcolor="#FFFFFF"/>
		</conditionalStyle>
	</style>
	<parameter name="project" class="java.lang.String"/>
	<parameter name="allowsSettled" class="java.lang.Boolean"/>
	<parameter name="allowsUntestable" class="java.lang.Boolean"/>
	<parameter name="campaign" class="java.lang.String"/>
	<parameter name="iteration" class="java.lang.String"/>
	<parameter name="testsuite" class="java.lang.String"/>
	<parameter name="rowcount" class="java.util.Map" isForPrompting="false"/>
	<field name="reference" class="java.lang.String"/>
	<field name="requirementName" class="java.lang.String"/>
	<field name="criticality" class="java.lang.String"/>
	<field name="success" class="java.lang.Long"/>
	<field name="failure" class="java.lang.Long"/>
	<field name="blocked" class="java.lang.Long"/>
	<field name="untestable" class="java.lang.Long"/>
	<field name="running" class="java.lang.Long"/>
	<field name="ready" class="java.lang.Long"/>
	<field name="settled" class="java.lang.Long"/>
	<variable name="REQUIREMENT_CRITICALITY" class="java.lang.String">
		<variableExpression><![CDATA[($F{criticality}.equals("CRITICAL") ? $R{report.qualitativecoverage.critical} :
	    ($F{criticality}.equals("MAJOR") ? $R{report.qualitativecoverage.major} :
	       ($F{criticality}.equals("MINOR") ? $R{report.qualitativecoverage.minor} : $R{report.qualitativecoverage.undefined}
	       )
	    )
	)]]></variableExpression>
	</variable>
	<detail>
		<band height="20">
			<printWhenExpression><![CDATA[( $P{rowcount}.put( "rowcount", (Integer)($P{rowcount}.get("rowcount")) + Integer.valueOf(1) ) != null || java.lang.Boolean.TRUE )]]></printWhenExpression>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="0" y="0" width="99" height="20" uuid="69278726-b8ba-4dbe-be1f-89c32b0adf41">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{project}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="99" y="0" width="99" height="20" uuid="680ba4bf-8e62-4333-a89a-3d3b7c61c6cb">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{campaign}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="198" y="0" width="99" height="20" uuid="3996ae76-ef3d-4014-83be-8e1ab5e8f806">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{iteration}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="297" y="0" width="100" height="20" uuid="9008aace-375a-48e3-8eb7-ffae9d5a2364">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{testsuite}==null? "--" : $P{testsuite}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="397" y="0" width="100" height="20" uuid="983875a9-da2c-483a-8657-a38224ef05fd">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{reference}.equals("")? $F{requirementName} : $F{reference}+" - "+$F{requirementName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="497" y="0" width="50" height="20" uuid="adcdd70e-3a3e-4a74-b3e4-ab765c13e342">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$V{REQUIREMENT_CRITICALITY}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="547" y="0" width="27" height="20" uuid="ee1ef603-2c39-4a5b-835c-facab6008874">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{success}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="574" y="0" width="27" height="20" uuid="03b409f3-7560-411c-a780-7af260c8ef6a">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{allowsSettled} ? $F{settled} : new String("--")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="601" y="0" width="27" height="20" uuid="42b34e75-8f0e-438e-a1ca-c4c5e0bc2040">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{failure}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="628" y="0" width="27" height="20" uuid="95a06c4f-f92b-4bd6-83fc-0c76d67b9378">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{blocked}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="655" y="0" width="27" height="20" uuid="1864a67f-9496-48ff-96ac-7665577aed2d">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$P{allowsUntestable} ? $F{untestable} : new String("--")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="682" y="0" width="27" height="20" uuid="652c20b6-24d8-48c9-b8eb-3d975913cc8f">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{running}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement style="small_name" stretchType="RelativeToBandHeight" x="709" y="0" width="27" height="20" uuid="483d0806-2d31-4f45-b1e7-6698b97f0dfb">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box leftPadding="5"/>
				<textFieldExpression><![CDATA[$F{ready}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
