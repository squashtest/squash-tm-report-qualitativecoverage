SELECT DISTINCT
	clnode.cln_id, 
	clnode.name, 
	clnode.project_id
FROM CAMPAIGN_LIBRARY_NODE AS clnode 
JOIN CAMPAIGN AS cmp ON clnode.cln_id = cmp.cln_id
WHERE clnode.project_id = :id