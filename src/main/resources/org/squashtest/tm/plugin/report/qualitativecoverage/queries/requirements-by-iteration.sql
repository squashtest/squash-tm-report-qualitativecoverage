SELECT DISTINCT
rv.requirement_id AS REQUIREMENT_ID,
rs.name AS REQUIREMENT_NAME,
rv.reference AS REQUIREMENT_REF,
rv.version_number AS REQUIREMENT_VERSION_NUMBER,
rv.criticality AS CRITICALITY,
COALESCE(success.counts,0) AS NB_EXECUTIONS_SUCCESS,
COALESCE(failure.counts,0) AS NB_EXECUTIONS_FAILURE,
COALESCE(untestable.counts,0) AS NB_EXECUTIONS_UNTESTABLE,
COALESCE(blocked.counts,0) AS NB_EXECUTIONS_BLOCKED,
COALESCE(running.counts,0) AS NB_EXECUTIONS_RUNNING,
COALESCE(ready.counts,0) AS NB_EXECUTIONS_READY,
COALESCE(settled.counts,0) AS NB_EXECUTIONS_SETTLED
FROM REQUIREMENT_VERSION AS rv
JOIN RESOURCE AS rs ON rv.res_id = rs.res_id
JOIN REQUIREMENT_VERSION_COVERAGE AS rvc ON rvc.verified_req_version_id = rs.res_id
JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE (itpi.execution_status='SUCCESS' OR itpi.execution_status='WARNING')
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS success
		ON success.id = rs.res_id  
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE itpi.execution_status='FAILURE'
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS failure
		ON failure.id = rs.res_id  
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE itpi.execution_status='UNTESTABLE'
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS untestable
		ON untestable.id = rs.res_id 
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE itpi.execution_status='SETTLED'
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS settled
		ON settled.id = rs.res_id  
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE (itpi.execution_status='BLOCKED' OR itpi.execution_status='ERROR') 
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS blocked
		ON blocked.id = rs.res_id  
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE itpi.execution_status='RUNNING'
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS running
		ON running.id = rs.res_id  
LEFT OUTER JOIN (SELECT DISTINCT
		Count(*) AS COUNTS,
		rvc.verified_req_version_id AS id
		FROM REQUIREMENT_VERSION_COVERAGE AS rvc
		JOIN TEST_CASE AS tc ON tc.tcln_id =  rvc.verifying_test_case_id
		JOIN ITERATION_TEST_PLAN_ITEM itpi ON itpi.tcln_id = tc.tcln_id
		JOIN ITEM_TEST_PLAN_LIST itpl ON itpl.item_test_plan_id = itpi.item_test_plan_id
		WHERE itpi.execution_status='READY'
		AND itpl.iteration_id = :id
		GROUP BY rvc.verified_req_version_id) AS ready
		ON ready.id = rs.res_id  
WHERE itpl.iteration_id = :id
ORDER BY rs.name ASC