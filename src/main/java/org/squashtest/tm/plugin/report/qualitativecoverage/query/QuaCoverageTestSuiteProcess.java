/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageRequirementBean;
import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageTestSuiteBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuaCoverageTestSuiteProcess extends AbstractItemProcess {

	private List<QuaCoverageTestSuiteBean> testSuiteBeans;

	public List<QuaCoverageTestSuiteBean> getTestSuites(QueryContext ctx, Long iterationId) {

		testSuiteBeans = new ArrayList<QuaCoverageTestSuiteBean>();

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", iterationId);
		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("testSuitesByIteration")), parameters);
		convertResultToTestSuiteBean(result, ctx);

		return testSuiteBeans;
	}

	private void convertResultToTestSuiteBean(List<Object[]> result, QueryContext ctx) {

		for (Object[] row : result) {

			QuaCoverageTestSuiteBean testSuiteBean = new QuaCoverageTestSuiteBean();

			testSuiteBean.setTestSuiteId(evaluateExpressionToLong(row[0]));
			testSuiteBean.setTestSuiteName(evaluateExpressionToString(row[1]));
			testSuiteBean.setRequirements(getRequirementBeans(ctx,testSuiteBean.getTestSuiteId()));

			testSuiteBeans.add(testSuiteBean);
		}
	}

	private List<QuaCoverageRequirementBean> getRequirementBeans(QueryContext ctx, Long testSuiteId){

		List<QuaCoverageRequirementBean> requirementBeans = new ArrayList<QuaCoverageRequirementBean>();
		QuaCoverageRequirementBean requirementBean;

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", testSuiteId);

		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("requirementsByTestSuite")), parameters);

		for (Object[] row : result) {

			requirementBean = new QuaCoverageRequirementBean();

			requirementBean.setRequirementId(evaluateExpressionToLong(row[0]));
			requirementBean.setRequirementName(evaluateExpressionToString(row[1]));
			requirementBean.setReference(evaluateExpressionToString(row[2]));
			requirementBean.setVersion(evaluateExpressionToString(row[3]));
			requirementBean.setCriticality(evaluateExpressionToString(row[4]));
			requirementBean.setSuccess(evaluateExpressionToLong(row[5]));
			requirementBean.setFailure(evaluateExpressionToLong(row[6]));
			requirementBean.setUntestable(evaluateExpressionToLong(row[7]));
			requirementBean.setBlocked(evaluateExpressionToLong(row[8]));
			requirementBean.setRunning(evaluateExpressionToLong(row[9]));
			requirementBean.setReady(evaluateExpressionToLong(row[10]));
			requirementBean.setSettled(evaluateExpressionToLong(row[11]));

			requirementBeans.add(requirementBean);
		}

		return requirementBeans;
	}
}
