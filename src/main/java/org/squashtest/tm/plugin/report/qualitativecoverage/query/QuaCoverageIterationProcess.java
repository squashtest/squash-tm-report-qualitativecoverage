/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageIterationBean;
import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageRequirementBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuaCoverageIterationProcess extends AbstractItemProcess {

	private List<QuaCoverageIterationBean> iterationBeans;

	public List<QuaCoverageIterationBean> getIterations(QueryContext ctx, Long campaignId) {

		iterationBeans = new ArrayList<QuaCoverageIterationBean>();

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", campaignId);
		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("iterationsByCampaign")), parameters);
		convertResultToIterationBean(result,ctx);
		return iterationBeans;
	}

	private void convertResultToIterationBean(List<Object[]> result, QueryContext ctx) {

		for (Object[] row : result) {

			QuaCoverageIterationBean iterationBean = new QuaCoverageIterationBean();

			iterationBean.setIterationId(evaluateExpressionToLong(row[0]));
			iterationBean.setIterationName(evaluateExpressionToString(row[1]));
			iterationBean.setIterationOrder(evaluateExpressionToLong(row[2]));
			iterationBean.setRequirements(getRequirementBeans(ctx,iterationBean.getIterationId()));

			QuaCoverageTestSuiteProcess testSuiteProcess = new QuaCoverageTestSuiteProcess();
			iterationBean.setTestSuites(testSuiteProcess.getTestSuites(ctx, iterationBean.getIterationId()));

			iterationBeans.add(iterationBean);
		}
	}


	private List<QuaCoverageRequirementBean> getRequirementBeans(QueryContext ctx, Long iterationId){

		List<QuaCoverageRequirementBean> requirementBeans = new ArrayList<QuaCoverageRequirementBean>();
		QuaCoverageRequirementBean requirementBean;

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", iterationId);

		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("requirementsByIteration")), parameters);

		for (Object[] row : result) {

			requirementBean = new QuaCoverageRequirementBean();

			requirementBean.setRequirementId(evaluateExpressionToLong(row[0]));
			requirementBean.setRequirementName(evaluateExpressionToString(row[1]));
			requirementBean.setReference(evaluateExpressionToString(row[2]));
			requirementBean.setVersion(evaluateExpressionToString(row[3]));
			requirementBean.setCriticality(evaluateExpressionToString(row[4]));
			requirementBean.setSuccess(evaluateExpressionToLong(row[5]));
			requirementBean.setFailure(evaluateExpressionToLong(row[6]));
			requirementBean.setUntestable(evaluateExpressionToLong(row[7]));
			requirementBean.setBlocked(evaluateExpressionToLong(row[8]));
			requirementBean.setRunning(evaluateExpressionToLong(row[9]));
			requirementBean.setReady(evaluateExpressionToLong(row[10]));
			requirementBean.setSettled(evaluateExpressionToLong(row[11]));
			requirementBeans.add(requirementBean);
		}

		return requirementBeans;
	}
}
