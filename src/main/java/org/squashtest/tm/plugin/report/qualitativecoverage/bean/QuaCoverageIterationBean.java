/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.bean;

import java.util.List;

public class QuaCoverageIterationBean extends QuaCoverageRequirementVerifier{


	private Long iterationId;

	private String iterationName;

	private Long iterationOrder;

	private List<QuaCoverageTestSuiteBean> testSuites;

	private List<QuaCoverageTestPlanItemBean> testPlanItems;

	public Long getIterationId() {
		return iterationId;
	}

	public void setIterationId(Long iterationId) {
		this.iterationId = iterationId;
	}

	public String getIterationName() {
		return iterationName;
	}

	public void setIterationName(String iterationName) {
		this.iterationName = iterationName;
	}

	public List<QuaCoverageTestSuiteBean> getTestSuites() {
		return testSuites;
	}

	public void setTestSuites(List<QuaCoverageTestSuiteBean> testSuites) {
		this.testSuites = testSuites;
	}

	public List<QuaCoverageTestPlanItemBean> getTestPlanItems() {
		return testPlanItems;
	}

	public void setTestPlanItems(List<QuaCoverageTestPlanItemBean> testPlanItems) {
		this.testPlanItems = testPlanItems;
	}

	public Long getIterationOrder() {
		return iterationOrder+1L;
	}

	public void setIterationOrder(Long iterationOrder) {
		this.iterationOrder = iterationOrder;
	}

}
