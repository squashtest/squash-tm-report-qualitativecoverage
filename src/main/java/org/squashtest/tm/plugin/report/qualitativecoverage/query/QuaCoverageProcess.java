/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageProjectBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuaCoverageProcess extends AbstractItemProcess {

	private List<QuaCoverageProjectBean> projectBeans;
	private Collection<Integer> milestoneIds;

	public List<QuaCoverageProjectBean> getProjects(QueryContext ctx, Collection<String> projectIds) {

		projectBeans = new ArrayList<QuaCoverageProjectBean>();
		for (String id : projectIds) {

			Map<String, Object> parameters = new HashMap<String, Object>();
			Long idAsLong = evaluateExpressionToLong(id);
			parameters.put("id", idAsLong);
			List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("projectsById")), parameters);
			convertResultToProjectBean(result, ctx);
		}

		return projectBeans;
	}

	private void convertResultToProjectBean(List<Object[]> result, QueryContext ctx) {

		for (Object[] row : result) {

			QuaCoverageProjectBean projectBean = new QuaCoverageProjectBean();

			projectBean.setProjectId(evaluateExpressionToLong(row[0]));
			projectBean.setProjectName(evaluateExpressionToString(row[1]));

			QuaCoverageCampaignProcess campaignProcess = new QuaCoverageCampaignProcess();
			campaignProcess.setMilestoneIds(milestoneIds);

			projectBean.setCampaigns(campaignProcess.getCampaigns(ctx, projectBean.getProjectId()));

			List<String> disabledExecutionStatus = getDisabledExecutionStatus(ctx, projectBean.getProjectId());
			projectBean.setDisabledExecutionStatus(disabledExecutionStatus);

			if (!projectBean.getCampaigns().isEmpty()) {
				projectBeans.add(projectBean);
			}
		}

	}

	public void setMilestoneIds(Collection<Integer> milestoneIds) {
		this.milestoneIds = milestoneIds;
	}

	private List<String> getDisabledExecutionStatus(QueryContext ctx, Long projectId) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", projectId);
		List<String> disabledExecutionStatus = ctx.getRunner().executeSelect(
				loadQuery(ctx.get("projectDisabledStatuses")), parameters);
		return disabledExecutionStatus;
	}
}
