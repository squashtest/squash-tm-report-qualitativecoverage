/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.springframework.core.io.Resource;
import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageCampaignBean;
import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageRequirementBean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuaCoverageCampaignProcess extends AbstractItemProcess {

	private List<QuaCoverageCampaignBean> campaignBeans;
	private Collection<Integer> milestoneIds;

	public List<QuaCoverageCampaignBean> getCampaigns(QueryContext ctx, Long projectId) {

		campaignBeans = new ArrayList<QuaCoverageCampaignBean>();

		Resource query = null;
		Map<String, Object> parameters = new HashMap<String, Object>();

		// set-up the query and parameters depending on the milestone mode or not
		if (milestoneIds == null) {
			query = ctx.get("campaignsByProject");
			parameters.put("id", projectId);
		} else {
			query = ctx.get("campaignsByProjectAndMilestones");
			parameters.put("id", projectId);
			parameters.put("milestones", milestoneIds);
		}

		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(query), parameters);
		convertResultToCampaignBean(result, ctx);

		return campaignBeans;
	}

	private void convertResultToCampaignBean(List<Object[]> result, QueryContext ctx) {

		for (Object[] row : result) {

			QuaCoverageCampaignBean campaignBean = new QuaCoverageCampaignBean();

			campaignBean.setCampaignId(evaluateExpressionToLong(row[0]));
			campaignBean.setCampaignName(evaluateExpressionToString(row[1]));
			campaignBean.setRequirements(getRequirementBeans(ctx, campaignBean.getCampaignId()));

			QuaCoverageIterationProcess iterationProcess = new QuaCoverageIterationProcess();
			campaignBean.setIterations(iterationProcess.getIterations(ctx, campaignBean.getCampaignId()));

			campaignBeans.add(campaignBean);
		}
	}

	private List<QuaCoverageRequirementBean> getRequirementBeans(QueryContext ctx, Long campaignId) {

		List<QuaCoverageRequirementBean> requirementBeans = new ArrayList<QuaCoverageRequirementBean>();
		QuaCoverageRequirementBean requirementBean;

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", campaignId);

		List<Object[]> result = ctx.getRunner().executeSelect(loadQuery(ctx.get("requirementsByCampaign")), parameters);

		for (Object[] row : result) {

			requirementBean = new QuaCoverageRequirementBean();

			requirementBean.setRequirementId(evaluateExpressionToLong(row[0]));
			requirementBean.setRequirementName(evaluateExpressionToString(row[1]));
			requirementBean.setReference(evaluateExpressionToString(row[2]));
			requirementBean.setVersion(evaluateExpressionToString(row[3]));
			requirementBean.setCriticality(evaluateExpressionToString(row[4]));
			requirementBean.setSuccess(evaluateExpressionToLong(row[5]));
			requirementBean.setFailure(evaluateExpressionToLong(row[6]));
			requirementBean.setUntestable(evaluateExpressionToLong(row[7]));
			requirementBean.setBlocked(evaluateExpressionToLong(row[8]));
			requirementBean.setRunning(evaluateExpressionToLong(row[9]));
			requirementBean.setReady(evaluateExpressionToLong(row[10]));
			requirementBean.setSettled(evaluateExpressionToLong(row[11]));

			requirementBeans.add(requirementBean);
		}

		return requirementBeans;
	}

	public void setMilestoneIds(Collection<Integer> milestoneIds) {
		this.milestoneIds = milestoneIds;
	}
}
