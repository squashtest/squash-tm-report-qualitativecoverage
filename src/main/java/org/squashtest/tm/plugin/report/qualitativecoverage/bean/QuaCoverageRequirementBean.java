/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.bean;

public class QuaCoverageRequirementBean {

	private Long requirementId = 0L;

	private String requirementName ="";

	private String reference ="";

	private String version ="";

	private String criticality ="";

	private Long success= 0L;

	private Long failure= 0L;

	private Long blocked= 0L;

	private Long untestable= 0L;

	private Long running= 0L;

	private Long ready= 0L;

	private Long settled= 0L;

	public Long getAggregatedSuccess() {
		return success + settled;
	}

	public Long getAggregatedFailure() {
		return failure + blocked + untestable;
	}

	public Long getAggregatedNonExec(){
		return running + ready;
	}

	private String computeStatus(){

		String status;

		if(this.getAggregatedSuccess() > 0){
			status = "CHECKED";
		} else if(this.getAggregatedFailure() > 0){
			status = "INCONCLUSIVE";
		} else {
			status = "NOT_CHECKED";
		}

		return status;
	}

	public Long getRequirementId() {
		return requirementId;
	}

	public void setRequirementId(Long requirementId) {
		this.requirementId = requirementId;
	}

	public String getRequirementName() {
		return requirementName;
	}

	public void setRequirementName(String requirementName) {
		this.requirementName = requirementName;
	}

	public String getStatus() {
		return computeStatus();
	}

	public Long getSuccess() {
		return success;
	}

	public void setSuccess(long success) {
		this.success = success;
	}

	public Long getFailure() {
		return failure;
	}

	public void setFailure(long failure) {
		this.failure = failure;
	}

	public Long getBlocked() {
		return blocked;
	}

	public void setBlocked(long blocked) {
		this.blocked = blocked;
	}

	public Long getUntestable() {
		return untestable;
	}

	public void setUntestable(long untestable) {
		this.untestable = untestable;
	}

	public Long getRunning() {
		return running;
	}

	public void setRunning(long running) {
		this.running = running;
	}

	public Long getReady() {
		return ready;
	}

	public void setReady(long ready) {
		this.ready = ready;
	}

	public Long getSettled() {
		return settled;
	}

	public void setSettled(long settled) {
		this.settled = settled;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCriticality() {
		return criticality;
	}

	public void setCriticality(String criticality) {
		this.criticality = criticality;
	}
}
