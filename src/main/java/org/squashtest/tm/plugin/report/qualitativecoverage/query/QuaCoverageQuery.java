/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.springframework.core.io.Resource;
import org.squashtest.tm.api.report.criteria.Criteria;
import org.squashtest.tm.api.report.query.ReportQuery;
import org.squashtest.tm.api.repository.SqlQueryRunner;
import org.squashtest.tm.plugin.report.qualitativecoverage.bean.QuaCoverageProjectBean;
import org.squashtest.tm.service.security.PermissionEvaluationService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class QuaCoverageQuery implements ReportQuery {

	private static final String CRIT_MILESTONES = "milestones";

	private static final String CRIT_PROJ_IDS = "projectIds";

	private static final String GET_MILESTONE_LABEL = "select LABEL from MILESTONE where MILESTONE_ID = :milestoneId";

	private static final String CRIT_PROJECT_PICKER = "PROJECT_PICKER";

	private Resource requirementsByCampaign;

	private Resource requirementsByIteration;

	private Resource requirementsByTestSuite;

	private Resource projectsById;

	private Resource projectDisabledStatuses;

	private Resource allProjects;

	private Resource campaignsByProject;

	private Resource campaignsByProjectAndMilestones;

	private Resource iterationsByCampaign;

	private Resource testSuitesByIteration;

	private SqlQueryRunner runner;

	private PermissionEvaluationService permissionService;

	public void setPermissionEvaluationService(PermissionEvaluationService service) {
		this.permissionService = service;
	}

	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void executeQuery(Map<String, Criteria> criteria, Map<String, Object> model) {

		// ********* init the processing context ***********
		QuaCoverageProcess projectProcess = new QuaCoverageProcess();

		QueryContext projectCtx = new QueryContext();

		projectCtx.put("requirementsByCampaign", requirementsByCampaign);
		projectCtx.put("requirementsByIteration", requirementsByIteration);
		projectCtx.put("requirementsByTestSuite", requirementsByTestSuite);
		projectCtx.put("projectsById", projectsById);
		projectCtx.put("projectDisabledStatuses", projectDisabledStatuses);
		projectCtx.put("allProjects", allProjects);
		projectCtx.put("campaignsByProject", campaignsByProject);
		projectCtx.put("campaignsByProjectAndMilestones", campaignsByProjectAndMilestones);
		projectCtx.put("iterationsByCampaign", iterationsByCampaign);
		projectCtx.put("testSuitesByIteration", testSuitesByIteration);
		projectCtx.setRunner(runner);

		/* ************************************************************************
		 * The scope of the report. If it is project ids-based, restrict to the given projects. If it is
		 * milestone-based, all projects bound to milestone are included.
		 *
		 * ***********************************************************************
		 */

		String mode = (String) criteria.get("requirementsSelectionMode").getValue();

		Collection<String> projectIds = null;
		List<Integer> milestoneIds = null;

		if (mode.equals(CRIT_PROJECT_PICKER)) {
			projectIds = (List<String>) criteria.get(CRIT_PROJ_IDS).getValue();
		} else {
			milestoneIds = (List<Integer>) criteria.get(CRIT_MILESTONES).getValue();
			projectIds = new QuaCoverageProjectProcess().getProjectIds(projectCtx, milestoneIds);
		}

		Collection<String> accessibleProjectIds = filterAccessibleProject(projectIds);
		projectCtx.setCriteria(accessibleProjectIds);

		List<QuaCoverageProjectBean> result = Collections.emptyList();

		projectProcess.setMilestoneIds(milestoneIds);

		result = projectProcess.getProjects(projectCtx, accessibleProjectIds);

		// if milestone, also include the milestone label
		if (milestoneIds != null) {
			Integer milestoneId = milestoneIds.get(0);
			Map<String, Object> args = new HashMap<String, Object>();
			args.put("milestoneId", milestoneId);
			String milestoneLabel = runner.executeUniqueSelect(GET_MILESTONE_LABEL, args);
			model.put("milestoneLabel", milestoneLabel);
		}

		model.put("data", result);
	}

	public void setRequirementsByCampaign(Resource requirementsByCampaign) {
		this.requirementsByCampaign = requirementsByCampaign;
	}

	public void setRequirementsByIteration(Resource requirementsByIteration) {
		this.requirementsByIteration = requirementsByIteration;
	}

	public void setRequirementsByTestSuite(Resource requirementsByTestSuite) {
		this.requirementsByTestSuite = requirementsByTestSuite;
	}

	public void setProjectsById(Resource projectsById) {
		this.projectsById = projectsById;
	}

	public void setAllProjects(Resource allProjects) {
		this.allProjects = allProjects;
	}

	public void setCampaignsByProject(Resource campaignsByProject) {
		this.campaignsByProject = campaignsByProject;
	}

	public void setCampaignsByProjectAndMilestones(Resource campaignsByProjectAndMilestones) {
		this.campaignsByProjectAndMilestones = campaignsByProjectAndMilestones;
	}

	public void setIterationsByCampaign(Resource iterationsByCampaign) {
		this.iterationsByCampaign = iterationsByCampaign;
	}

	public void setTestSuitesByIteration(Resource testSuitesByIteration) {
		this.testSuitesByIteration = testSuitesByIteration;
	}

	public void setProjectDisabledStatuses(Resource projectDisabledStatuses) {
		this.projectDisabledStatuses = projectDisabledStatuses;
	}

	private Collection<String> filterAccessibleProject(Collection<String> projectIds) {
		Collection<String> filtered = new LinkedList<String>();
		for (String projectId : projectIds) {
			if (permissionService.hasRoleOrPermissionOnObject("ROLE_ADMIN", "READ", Long.decode(projectId),
				"org.squashtest.tm.domain.project.Project")) {
				filtered.add(projectId);
			}
		}
		return filtered;
	}
}
