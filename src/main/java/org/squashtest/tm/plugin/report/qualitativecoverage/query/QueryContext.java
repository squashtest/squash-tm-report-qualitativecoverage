/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.springframework.core.io.Resource;
import org.squashtest.tm.api.repository.SqlQueryRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Map that hold Parameters and resources for quering
 * squash database and filling reports
 *
 */
public class QueryContext extends HashMap<String, Resource>{

	private static final long serialVersionUID = 7620885921647139825L;

	/**
	 * The runner service to execute queries
	 */
	private SqlQueryRunner runner;

	/**
	 * Collection of ids to restrict query (where clause)
	 */
	private Collection<?> criteria;

	/**
	 * Options for report printing (optional sections ...)
	 */
	private Map<String,Boolean> reportOptions;


	/**
	 * Options for queries (for filtering)
	 */
	private Map<String,Boolean> queryOptions;


	/**
	 * Set to hold ids of ancestors (first part of the query) :
	 * used to remove duplicate entries
	 * when user select to subnodes with the same ancestors
	 */
	private Set<Long> ancestors;

	/**
	 * A List that contains data to compute paragraph (global scope).
	 */
	private List<Integer> paragraphLevel = new ArrayList<Integer>();


	public SqlQueryRunner getRunner() {
		return runner;
	}

	public void setRunner(SqlQueryRunner runner) {
		this.runner = runner;
	}

	public Collection<?> getCriteria() {
		return criteria;
	}

	public void setCriteria(Collection<?> criteria) {
		this.criteria = criteria;
	}

	public void setAncestors(Set<Long> ancestors) {
		this.ancestors = ancestors;
	}

	public Set<Long> getAncestors() {
		return ancestors;
	}

	public List<Integer> getParagraphLevel() {
		return paragraphLevel;
	}

	public void setParagraphLevel(List<Integer> paragraphLevel) {
		this.paragraphLevel = paragraphLevel;
	}

	public void setReportOptions(Map<String,Boolean> reportOptions) {
		this.reportOptions = reportOptions;
	}

	public Map<String,Boolean> getReportOptions() {
		return reportOptions;
	}

	public void setQueryOptions(Map<String,Boolean> queryOptions) {
		this.queryOptions = queryOptions;
	}

	public Map<String,Boolean> getQueryOptions() {
		return queryOptions;
	}

}
