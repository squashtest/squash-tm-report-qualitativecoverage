/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class QuaCoverageRequirementVerifier {

	private List<QuaCoverageRequirementBean> requirements = new ArrayList<QuaCoverageRequirementBean>();
	private Map<String,Long> statistics = new HashMap<String,Long>();

	//Critical
	private Long numberOfNontestedCriticalRequirements  = 0L;
	private Long numberOfTestedCriticalRequirements  = 0L;
	private Long totalNumberOfCriticalRequirements  = 0L;
	private Long numberOfVerifiedCriticalRequirements  = 0L;

	//Major
	private Long numberOfNontestedMajorRequirements  = 0L;
	private Long numberOfTestedMajorRequirements  = 0L;
	private Long totalNumberOfMajorRequirements  = 0L;
	private Long numberOfVerifiedMajorRequirements  = 0L;

	//Minor
	private Long numberOfNontestedMinorRequirements  = 0L;
	private Long numberOfTestedMinorRequirements  = 0L;
	private Long totalNumberOfMinorRequirements  = 0L;
	private Long numberOfVerifiedMinorRequirements  = 0L;

	//Undefined
	private Long numberOfNontestedUndefinedRequirements  = 0L;
	private Long numberOfTestedUndefinedRequirements  = 0L;
	private Long totalNumberOfUndefinedRequirements = 0L;
	private Long numberOfVerifiedUndefinedRequirements  = 0L;

	//Total
	private Long numberOfNontestedRequirements  = 0L;
	private Long numberOfTestedRequirements  = 0L;
	private Long totalNumberOfRequirements = 0L;
	private Double percentageOfProgress = .0;
	private Long totalNumberOfVerifiedRequirements  = 0L;


	public List<QuaCoverageRequirementBean> getRequirements() {
		return requirements;
	}

	public void setRequirements(List<QuaCoverageRequirementBean> requirements) {
		this.requirements = requirements;
		this.calculateStatistics();
	}


	private void calculateStatistics(){

		for(QuaCoverageRequirementBean req : requirements){

			String key = req.getStatus()+":"+req.getCriticality();

			if(this.statistics.containsKey(key)){
				this.statistics.put(key, this.statistics.get(key)+1);
			}
			else{
				this.statistics.put(key, 1L);
			}
		}

		//Critical
		setNumberOfNontestedCriticalRequirements(getLongForKey("NOT_CHECKED:CRITICAL"));
		setNumberOfTestedCriticalRequirements(getLongForKey("CHECKED:CRITICAL")+getLongForKey("INCONCLUSIVE:CRITICAL"));
		setTotalNumberOfCriticalRequirements(this.numberOfNontestedCriticalRequirements+this.numberOfTestedCriticalRequirements);
		setNumberOfVerifiedCriticalRequirements(getLongForKey("CHECKED:CRITICAL"));

		//Major
		setNumberOfNontestedMajorRequirements(getLongForKey("NOT_CHECKED:MAJOR"));
		setNumberOfTestedMajorRequirements(getLongForKey("CHECKED:MAJOR")+getLongForKey("INCONCLUSIVE:MAJOR"));
		setTotalNumberOfMajorRequirements(this.numberOfNontestedMajorRequirements+this.numberOfTestedMajorRequirements);
		setNumberOfVerifiedMajorRequirements(getLongForKey("CHECKED:MAJOR"));

		//Minor
		setNumberOfNontestedMinorRequirements(getLongForKey("NOT_CHECKED:MINOR"));
		setNumberOfTestedMinorRequirements(getLongForKey("CHECKED:MINOR")+getLongForKey("INCONCLUSIVE:MINOR"));
		setTotalNumberOfMinorRequirements(this.numberOfNontestedMinorRequirements+this.numberOfTestedMinorRequirements);
		setNumberOfVerifiedMinorRequirements(getLongForKey("CHECKED:MINOR"));

		//Undefined
		setNumberOfNontestedUndefinedRequirements(getLongForKey("NOT_CHECKED:UNDEFINED"));
		setNumberOfTestedUndefinedRequirements(getLongForKey("CHECKED:UNDEFINED")+getLongForKey("INCONCLUSIVE:UNDEFINED"));
		setTotalNumberOfUndefinedRequirements(this.numberOfNontestedUndefinedRequirements+this.numberOfTestedUndefinedRequirements);
		setNumberOfVerifiedUndefinedRequirements(getLongForKey("CHECKED:UNDEFINED"));

		//Total
		setNumberOfNontestedRequirements(
				this.numberOfNontestedCriticalRequirements+
				this.numberOfNontestedMajorRequirements+
				this.numberOfNontestedMinorRequirements+
				this.numberOfNontestedUndefinedRequirements);

		setNumberOfTestedRequirements(
				this.numberOfTestedCriticalRequirements+
				this.numberOfTestedMajorRequirements+
				this.numberOfTestedMinorRequirements+
				this.numberOfTestedUndefinedRequirements);

		setTotalNumberOfRequirements(
				this.numberOfNontestedRequirements+
				this.numberOfTestedRequirements);

		setPercentageOfProgress(
				(this.numberOfTestedRequirements.doubleValue() /
						this.totalNumberOfRequirements.doubleValue())*100.0);

		setTotalNumberOfVerifiedRequirements(
				this.numberOfVerifiedCriticalRequirements+
				this.numberOfVerifiedMajorRequirements+
				this.numberOfVerifiedMinorRequirements+
				this.numberOfVerifiedUndefinedRequirements);
	}

	private Long getLongForKey(String key){

		Long result = 0L;

		if(this.statistics.containsKey(key)){
			result = this.statistics.get(key);
		}

		return result;
	}

	public Long getNumberOfNontestedCriticalRequirements() {
		return numberOfNontestedCriticalRequirements;
	}

	public void setNumberOfNontestedCriticalRequirements(
			Long numberOfNontestedCriticalRequirements) {
		this.numberOfNontestedCriticalRequirements = numberOfNontestedCriticalRequirements;
	}

	public Long getNumberOfTestedCriticalRequirements() {
		return numberOfTestedCriticalRequirements;
	}

	public void setNumberOfTestedCriticalRequirements(
			Long numberOfTestedCriticalRequirements) {
		this.numberOfTestedCriticalRequirements = numberOfTestedCriticalRequirements;
	}

	public Long getTotalNumberOfCriticalRequirements() {
		return totalNumberOfCriticalRequirements;
	}

	public void setTotalNumberOfCriticalRequirements(
			Long totalNumberOfCriticalRequirements) {
		this.totalNumberOfCriticalRequirements = totalNumberOfCriticalRequirements;
	}

	public Long getNumberOfVerifiedCriticalRequirements() {
		return numberOfVerifiedCriticalRequirements;
	}

	public void setNumberOfVerifiedCriticalRequirements(
			Long numberOfVerifiedCriticalRequirements) {
		this.numberOfVerifiedCriticalRequirements = numberOfVerifiedCriticalRequirements;
	}

	public Long getNumberOfNontestedMajorRequirements() {
		return numberOfNontestedMajorRequirements;
	}

	public void setNumberOfNontestedMajorRequirements(
			Long numberOfNontestedMajorRequirements) {
		this.numberOfNontestedMajorRequirements = numberOfNontestedMajorRequirements;
	}

	public Long getNumberOfTestedMajorRequirements() {
		return numberOfTestedMajorRequirements;
	}

	public void setNumberOfTestedMajorRequirements(
			Long numberOfTestedMajorRequirements) {
		this.numberOfTestedMajorRequirements = numberOfTestedMajorRequirements;
	}

	public Long getTotalNumberOfMajorRequirements() {
		return totalNumberOfMajorRequirements;
	}

	public void setTotalNumberOfMajorRequirements(
			Long totalNumberOfMajorRequirements) {
		this.totalNumberOfMajorRequirements = totalNumberOfMajorRequirements;
	}

	public Long getNumberOfVerifiedMajorRequirements() {
		return numberOfVerifiedMajorRequirements;
	}

	public void setNumberOfVerifiedMajorRequirements(
			Long numberOfVerifiedMajorRequirements) {
		this.numberOfVerifiedMajorRequirements = numberOfVerifiedMajorRequirements;
	}

	public Long getNumberOfNontestedMinorRequirements() {
		return numberOfNontestedMinorRequirements;
	}

	public void setNumberOfNontestedMinorRequirements(
			Long numberOfNontestedMinorRequirements) {
		this.numberOfNontestedMinorRequirements = numberOfNontestedMinorRequirements;
	}

	public Long getNumberOfTestedMinorRequirements() {
		return numberOfTestedMinorRequirements;
	}

	public void setNumberOfTestedMinorRequirements(
			Long numberOfTestedMinorRequirements) {
		this.numberOfTestedMinorRequirements = numberOfTestedMinorRequirements;
	}

	public Long getTotalNumberOfMinorRequirements() {
		return totalNumberOfMinorRequirements;
	}

	public void setTotalNumberOfMinorRequirements(
			Long totalNumberOfMinorRequirements) {
		this.totalNumberOfMinorRequirements = totalNumberOfMinorRequirements;
	}

	public Long getNumberOfVerifiedMinorRequirements() {
		return numberOfVerifiedMinorRequirements;
	}

	public void setNumberOfVerifiedMinorRequirements(
			Long numberOfVerifiedMinorRequirements) {
		this.numberOfVerifiedMinorRequirements = numberOfVerifiedMinorRequirements;
	}

	public Long getNumberOfNontestedUndefinedRequirements() {
		return numberOfNontestedUndefinedRequirements;
	}

	public void setNumberOfNontestedUndefinedRequirements(
			Long numberOfNontestedUndefinedRequirements) {
		this.numberOfNontestedUndefinedRequirements = numberOfNontestedUndefinedRequirements;
	}

	public Long getNumberOfTestedUndefinedRequirements() {
		return numberOfTestedUndefinedRequirements;
	}

	public void setNumberOfTestedUndefinedRequirements(
			Long numberOfTestedUndefinedRequirements) {
		this.numberOfTestedUndefinedRequirements = numberOfTestedUndefinedRequirements;
	}

	public Long getTotalNumberOfUndefinedRequirements() {
		return totalNumberOfUndefinedRequirements;
	}

	public void setTotalNumberOfUndefinedRequirements(
			Long totalNumberOfUndefinedRequirements) {
		this.totalNumberOfUndefinedRequirements = totalNumberOfUndefinedRequirements;
	}

	public Long getNumberOfVerifiedUndefinedRequirements() {
		return numberOfVerifiedUndefinedRequirements;
	}

	public void setNumberOfVerifiedUndefinedRequirements(
			Long numberOfVerifiedUndefinedRequirements) {
		this.numberOfVerifiedUndefinedRequirements = numberOfVerifiedUndefinedRequirements;
	}

	public Long getNumberOfNontestedRequirements() {
		return numberOfNontestedRequirements;
	}

	public void setNumberOfNontestedRequirements(Long numberOfNontestedRequirements) {
		this.numberOfNontestedRequirements = numberOfNontestedRequirements;
	}

	public Long getNumberOfTestedRequirements() {
		return numberOfTestedRequirements;
	}

	public void setNumberOfTestedRequirements(Long numberOfTestedRequirements) {
		this.numberOfTestedRequirements = numberOfTestedRequirements;
	}

	public Long getTotalNumberOfRequirements() {
		return totalNumberOfRequirements;
	}

	public void setTotalNumberOfRequirements(Long totalNumberOfRequirements) {
		this.totalNumberOfRequirements = totalNumberOfRequirements;
	}

	public Long getPercentageOfProgress() {
		return percentageOfProgress.longValue();
	}

	public void setPercentageOfProgress(Double percentageOfProgress) {
		this.percentageOfProgress = percentageOfProgress;
	}

	public Long getTotalNumberOfVerifiedRequirements() {
		return totalNumberOfVerifiedRequirements;
	}

	public void setTotalNumberOfVerifiedRequirements(
			Long totalNumberOfVerifiedRequirements) {
		this.totalNumberOfVerifiedRequirements = totalNumberOfVerifiedRequirements;
	}
}
