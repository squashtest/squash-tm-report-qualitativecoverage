/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class AbstractItemProcess {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractItemProcess.class);

	protected String loadQuery(Resource query) {
		InputStream is;
		try {
			is = query.getInputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new Scanner(is, "UTF-8").useDelimiter("\\A").next();
	}

	private Locale currentLocale() {
		Locale current = LocaleContextHolder.getLocale();

		if (current == null) {
			current = Locale.getDefault();
			LOGGER.warn("No locale available from LocaleContextHolder, platform default will be used instead");
		}

		return current;
	}

	protected String convertDatetoString(Date date) {

		return DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
				DateFormat.SHORT, currentLocale()).format(date);

	}

	protected String evaluateExpressionToString(Object obj) {

		if (obj instanceof Date){
			return obj != null ? convertDatetoString((Date) obj): "";
		}

		return obj != null ? obj.toString() : "";

	}

	protected Boolean evaluateExpressionToBoolean(Object obj) {

		return ( (Integer) obj)>0;

	}


	protected Long evaluateExpressionToLong(Object obj) {

		return Long.valueOf(obj.toString());

	}

}
