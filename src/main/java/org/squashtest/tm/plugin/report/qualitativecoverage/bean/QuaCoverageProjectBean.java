/**
 * ====
 *         This file is part of the Squashtest platform.
 *         Copyright (C) 2010 - 2015 Henix, henix.fr
 *
 *         See the NOTICE file distributed with this work for additional
 *         information regarding copyright ownership.
 *
 *         This is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU Lesser General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         this software is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU Lesser General Public License for more details.
 *
 *         You should have received a copy of the GNU Lesser General Public License
 *         along with this software.  If not, see <http://www.gnu.org/licenses/>.
 * ====
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2010 - 2021 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.report.qualitativecoverage.bean;

import org.squashtest.tm.domain.execution.ExecutionStatus;

import java.util.List;

public class QuaCoverageProjectBean extends QuaCoverageRequirementVerifier{

	private Long projectId;

	private String projectName;

	private List<String> disabledExecutionStatus;


	private List<QuaCoverageCampaignBean> campaigns;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<QuaCoverageCampaignBean> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<QuaCoverageCampaignBean> campaigns) {
		this.campaigns = campaigns;
	}
	public boolean isAllowsSettled() {
		return ! disabledExecutionStatus.contains(ExecutionStatus.SETTLED.name());
	}

	public boolean isAllowsUntestable() {
		return ! disabledExecutionStatus.contains(ExecutionStatus.UNTESTABLE.name());
	}



	public void setDisabledExecutionStatus(List<String> disabledExecutionStatus) {
		this.disabledExecutionStatus = disabledExecutionStatus;

	}
}
